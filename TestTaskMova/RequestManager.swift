//
//  RequestManager.swift
//  TestTaskMova
//
//  Created by Kostiantyn Dzetsiuk on 7/2/19.
//  Copyright © 2019 Kostiantyn Dzetsiuk. All rights reserved.
//

import UIKit

class RequestManager: NSObject {

    func searchInputText(text: String, completion: @escaping (Bool) -> Void) {
        let url = giphyURLFromParameters (searchString: text)
        let session = URLSession.shared
        session.dataTask(with: url) { (data, response, error) in
            if let response = response {
                print(response)
            }
            guard let data = data else { return }
            print(data)
            do {
                let parsedData = try JSONDecoder().decode(DataStructure.self, from: data)
                if parsedData.data.count == 0 {
                    completion(false)
                } else {
                    let string = parsedData.data[0].images.original.url
                    self.fetchImage(url: string, imageName: text)
                }
            } catch {
                print(error)
            }
            }.resume()
    }
    
    private func fetchImage(url: String, imageName: String) {
        let imageURL = URL(string: url)
        URLSession.shared.dataTask(with: imageURL!) { (data, response, error) in
            if error == nil {
                let imageHandler = ImageHandler()
                let downloadImage = UIImage(data: data!)
                let name = String(imageName.filter { !" ".contains($0) })
                
                imageHandler.saveImageInDocumentDirectory(image: downloadImage!, imageName: name)
            }
            }.resume()
    }

    private func giphyURLFromParameters(searchString: String) -> URL {
        var components = URLComponents()
        components.scheme = Constants.GiphyURLParams.APIScheme
        components.host = Constants.GiphyURLParams.APIHost
        components.path = Constants.GiphyURLParams.APIPath
        
        components.queryItems = [URLQueryItem]()
        components.queryItems!.append(URLQueryItem(name: Constants.GiphyAPIKeys.APIKey, value: Constants.GiphyAPIValues.APIKey));
        components.queryItems!.append(URLQueryItem(name: Constants.GiphyAPIKeys.SearchParameter, value: searchString));
        
        return components.url!
    }
}
