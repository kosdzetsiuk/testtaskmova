//
//  ImageHandler.swift
//  TestTaskMova
//
//  Created by Kostiantyn Dzetsiuk on 7/2/19.
//  Copyright © 2019 Kostiantyn Dzetsiuk. All rights reserved.
//

import UIKit

class ImageHandler: NSObject {
    
    let dataBaseObject = DataBaseHandler()
    
    func saveImageInDocumentDirectory(image: UIImage, imageName: String) {
        let fileManager = FileManager.default
        let path = getDirectoryPath()
        let url = NSURL(string: path)
        let savingPath = ("imageDerictory/\(imageName.lowercased()).png")
        let imagePath = url!.appendingPathComponent("\(imageName.lowercased()).png")
        let urlString: String = imagePath!.absoluteString
        let imageData = image.jpegData(compressionQuality: 0.5)
        fileManager.createFile(atPath: urlString as String, contents: imageData, attributes: nil)
        saveInDBObject(name: imageName, imagePath: savingPath)
    }
    
    func getImageFromDocumentDirectoryForIndexPath(index: Int) -> UIImage{
        var image: UIImage?
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if let dirPath = paths.first {
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent(dataBaseObject.getImagePathFromHistoryForIndex(index: index))
            image = UIImage(contentsOfFile: imageURL.path)
        }
        if image == nil {
             image = UIImage(named: "Image")
        }
        return image!
    }
    
    private func getDirectoryPath() -> String {
        let fileManager = FileManager.default
        let path = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("imageDerictory")
        if !fileManager.fileExists(atPath: path) {
            try! fileManager.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
        }
        return path
    }
    
    private func saveInDBObject(name: String, imagePath: String) {
        dataBaseObject.resultImagePath = imagePath
        dataBaseObject.searchText = name
        dataBaseObject.saveHistoryInDataBase(object: dataBaseObject)
    }

}
