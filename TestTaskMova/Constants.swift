//
//  Constants.swift
//  TestTaskMova
//
//  Created by Kostiantyn Dzetsiuk on 7/1/19.
//  Copyright © 2019 Kostiantyn Dzetsiuk. All rights reserved.
//


import Foundation

struct Constants {
    
    struct GiphyURLParams {
        static let APIScheme = "https"
        static let APIHost = "api.giphy.com"
        static let APIPath = "/v1/stickers/search"
    }
    
    struct GiphyAPIKeys {
        static let APIKey = "api_key"
        static let SearchParameter = "q"
    }
    
    struct GiphyAPIValues {
        static let APIKey = "jxxx1CK00PDZyq87WA6Eg2Fhd9nMLrKx"
    }
    
}

struct DataStructure: Codable {
    let data: [Datum]
}

struct Datum: Codable {
    let images: Images
    
    enum CodingKeys: String, CodingKey {
        case images
    }
}

struct Images: Codable {
    let original: FixedHeight
    
    enum CodingKeys: String, CodingKey {
        case original
    }
}

struct FixedHeight: Codable {
    let url: String
}
