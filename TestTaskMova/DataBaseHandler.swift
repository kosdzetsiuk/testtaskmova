//
//  DataBaseHandler.swift
//  TestTaskMova
//
//  Created by Kostiantyn Dzetsiuk on 7/2/19.
//  Copyright © 2019 Kostiantyn Dzetsiuk. All rights reserved.
//

import Foundation
import RealmSwift

class DataBaseHandler: Object {

    @objc dynamic var searchText: String?
    @objc dynamic var resultImagePath: String?
    
    func saveHistoryInDataBase(object: Object){
        let realm = try! Realm()
        try! realm.write {
            realm.add(object)
        }
    }
    
    func getImagePathFromHistoryForIndex(index: Int) -> String {
        return getAllData()[index].resultImagePath!
    }
    
    func getSearchTextFromHistoryForIndex(index: Int) -> String {
        return getAllData()[index].searchText!
    }
    
    func getSearchHistoryCount() -> Int {
        return getAllData().count
    }
    
    func getSearchHistory() -> [DataBaseHandler] {
        return getAllData()
    }
    
    private func getAllData() -> [DataBaseHandler] {
        let realm = try! Realm()
        let result = Array(realm.objects(DataBaseHandler.self))
        return result.reversed()
    }
}
