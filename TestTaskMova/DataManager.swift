//
//  DataManager.swift
//  TestTaskMova
//
//  Created by Kostiantyn Dzetsiuk on 7/2/19.
//  Copyright © 2019 Kostiantyn Dzetsiuk. All rights reserved.
//

import UIKit

class DataManager: NSObject {

    let imageHandler = ImageHandler()
    let dataBaseHandler = DataBaseHandler()
    
    func getTableViewImageForIndex(index: Int) -> UIImage {
        return imageHandler.getImageFromDocumentDirectoryForIndexPath(index: index)
    }
    
    func getTableViewTextForIndex(index: Int) -> String {
        return dataBaseHandler.getSearchTextFromHistoryForIndex(index: index)
    }
    
    func getTableViewCount() -> Int {
        return dataBaseHandler.getSearchHistory().count
    }
    
}
