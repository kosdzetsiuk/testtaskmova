//
//  SearchBarClass.swift
//  TestTaskMova
//
//  Created by Kostiantyn Dzetsiuk on 7/2/19.
//  Copyright © 2019 Kostiantyn Dzetsiuk. All rights reserved.
//

import UIKit

class SearchBarClass: UISearchBar {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
