//
//  DelegatesExtension.swift
//  TestTaskMova
//
//  Created by Kostiantyn Dzetsiuk on 7/2/19.
//  Copyright © 2019 Kostiantyn Dzetsiuk. All rights reserved.
//

import Foundation
import UIKit

extension ViewController: UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    // MARK: TableViewDelegate/UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataManager.getTableViewCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = mainTable.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! CustomTableViewCell
        cell.nameLabel.text = dataManager.getTableViewTextForIndex(index: indexPath.row)
        cell.searchImage.image = dataManager.getTableViewImageForIndex(index: indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView,
      heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        view.endEditing(true)
    }
    
    // MARK: UISearchBarDelegate
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let session = RequestManager()
        session.searchInputText(text: searchBar.text!) { (success) in
            if !success {
                DispatchQueue.main.async { 
                    self.displayAlert()
                }
            }
        }
    }
    
    
    func displayAlert() {
        let alert = UIAlertController(title: "Nothing =(", message: "", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    func reload() {
        DispatchQueue.main.async {
            self.mainTable.reloadData()
        }
    }
}
