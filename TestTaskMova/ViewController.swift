//
//  ViewController.swift
//  TestTaskMova
//
//  Created by Kostiantyn Dzetsiuk on 7/1/19.
//  Copyright © 2019 Kostiantyn Dzetsiuk. All rights reserved.
//

import UIKit
import RealmSwift

class ViewController: UIViewController {

    let cellID = "cellID"
    let dataManager = DataManager()
    var notificationToken: NotificationToken?
    
    lazy var mainTable: UITableView = {
        let table = UITableView()
        table.translatesAutoresizingMaskIntoConstraints = false
        table.register(UITableViewCell.self, forCellReuseIdentifier: cellID)
        return table
    }()
    
    let searchBar = SearchBarClass()
    
    fileprivate func setupView(){
        view.addSubview(searchBar)
        view.addSubview(mainTable)
        
        searchBar.topAnchor.constraint(equalTo:view.safeAreaLayoutGuide.topAnchor).isActive = true
        searchBar.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        searchBar.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        
        mainTable.topAnchor.constraint(equalTo: searchBar.bottomAnchor, constant: 0).isActive = true
        mainTable.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        mainTable.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        mainTable.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        mainTable.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        mainTable.register(CustomTableViewCell.self, forCellReuseIdentifier: cellID)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        mainTable.delegate = self
        mainTable.dataSource = self
        searchBar.delegate = self
        searchBar.returnKeyType = UIReturnKeyType.search
        
        mainTable.register(CustomTableViewCell.self, forCellReuseIdentifier: cellID)
        
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        searchBar.placeholder = "Search"
        self.view.backgroundColor = UIColor.white
        
        let realm = try! Realm()
        notificationToken = realm.observe { notification, realm in
            self.reload()
        }
    }
    
    deinit {
        notificationToken?.invalidate()
    }
}

